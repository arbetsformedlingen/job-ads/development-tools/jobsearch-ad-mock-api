# LA Mock Api

[[_TOC_]]

## Description

Small [Flask](https://flask.palletsprojects.com/en/3.0.x/) api that mocks the three Ledigt Arbete endpoints used by the [jobsearch-importers](https://gitlab.com/arbetsformedlingen/job-ads/jobsearch-importers).
Code pushed to the repo will automatically deploy to [JobTech OpenShift testing cluster](https://console-openshift-console.apps.testing.services.jtech.se/k8s/ns/jobsearch-ad-mock-api-develop/core~v1~Pod)

### Endpoint example

* <https://la-mock-api-frankfurt-route-jobsearch-ad-mock-api-develop.apps.testing.services.jtech.se/annonser/23396767>
* <https://la-mock-api-frankfurt-route-jobsearch-ad-mock-api-develop.apps.testing.services.jtech.se/publiceradeannonser>
* <https://la-mock-api-frankfurt-route-jobsearch-ad-mock-api-develop.apps.testing.services.jtech.se/1583341616000>

## API

### Setup

Create a virtual environment or environment of choice and activate it.
Install necessary packages:

```sh
poetry install
```

### Start the API

To run the api locally, set the environment variable as `set FLASK_APP=la_mock.api:app` and then type `poetry run flask run`.

### Alternatively - start

For debugging in an IDE, run/debug the file `la_mock/__init__.py`

When the API has started, endpoints will be available at:

* <http://localhost:5000/sokningar/publiceradeannonser>
* <http://localhost:5000/annonser/><ad_id>
* <http://localhost:5000/sokningar/andradeannonser/><timestamp\>
(timestamp won't matter, always returns empty)

## Add new ads

To add new ads to the api, first find the absolute path to the folder la_mock/resources/ads. Then set an environment variable:
`set AD_PATH=la_mock/resources/ads`

### Import a specific ad when you know its id

Import an ad by running:
`poetry run import-ad <ad_id>`
The ad will be saved as a json file and the ad text printed out. You do not need to worry about whether ads are depublished, all ads will be treated as active when displayed by the api.

Make sure the ad has anonymized correctly.
If it has not, check in `la_mock/utils/anonymize/first_name.txt` and `la_mock/utils/anonymize/last_name.txt` if the first or last name is missing. Add them and run the `import-ad` command again for the same id to check if it worked. Finally store the files with git.

### Add multiple ads

Set the variable NUMBER_OF_RANDOM_ADS in `create_new_test_ads.py`
run `poetry run python create_new_test_ads.py`, it will download all current ad ids from Ledigt Arbete, pick the desired number on random and download these ads to individual json files

### Ads for comparison (100k ads)

These are ads that can be used for comparison of different implementations of jobsearch-importers or jobsearch-api.
There are a lot more of them because we don't want to draw conclusions of small differences that might be just because of which ads are included.
E.g.: a difference of 5 ads between two implementations is a lot if there are only 20 matching ads.
But the same difference is less important if there are 3000 ads returned for a query in one api and 2995 in the other.
Use the comparision ads from `Minio / jobbdata / testdata` (logon details are found in Vaultwarden). They are not in git because it's a lot of data.
Copy them to `la_mock/resources/compare_ads`
set environment variable `TEST_ADS=false` before starting the api.

### Creating new comparison ads (for use with compare-environments)

Run `poetry run python la_mock/utils/download_all_ads_from_ledigt_arbete.py` it will download all currently active ads from Ledigt Arbete. Re-run it in case of failures, it will pick up where it left.
When downloaded, they have to be anonymized (anonymizer in this repo is slow, use <https://gitlab.com/arbetsformedlingen/libraries/anonymisering>)
If you create new comparison ads, the query strings in compare-environments should be updated so that date queries matches the ads.

Todo: instructions and code for anonymization.

### Handle nyckelord/labels

The package installs a cli tool called `label` that can be used to add new labels to the test data.

Use `poetry run label randomize la_mock/resources/test_ads` to clear all old labels and add some new ones along with some mistakes (non spec data).

To add a new label to a random selection of the json files use `poetry run label add la_mock/resources/test_ads <LABEL>`. To modify the default 5% probability use the option `-p`, i.e. `-p 0.25` for 25%.

`poetry run label histogram la_mock/resources/test_ads` lists the distribution of labels present in json files.

## Historical API
Instructions for setting up AD-MOCK for historical ads follow this link here:
https://gitlab.com/arbetsformedlingen/job-ads/historical-ads/historical-api/-/blob/main/docs/testing.md?ref_type=heads#test-data-for-historical

