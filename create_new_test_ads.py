# -*- coding: utf-8 -*-
import random
import json
import logging
import requests
from la_mock.utils.ad_download import fetch_ad_and_store

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

LA_BOOTSTRAP_FEED_URL = 'https://www.arbetsformedlingen.se/rest/ledigtarbete/rest/af/v1/ledigtarbete/publikt/sokningar/publiceradeannonser/'

NUMBER_OF_RANDOM_ADS = 5000  # about 10%
EXTRA_AD_IDS = []  # ids as str
"""
Get all current ad ids from Ledigt Arbete
Select NUMBER_OF_RANDOM_ADS from those and create mock ads
"""


def get_ad_ids():
    response = requests.get(LA_BOOTSTRAP_FEED_URL)
    response.raise_for_status()
    list_of_current_ads = json.loads(response.content.decode('utf8'))['idLista']
    list_of_ids = []
    for item in list_of_current_ads:
        if not item['avpublicerad']:
            list_of_ids.append(item['annonsId'])
    return list_of_ids


def random_ad_ids(list_of_ids):
    return random.sample(list_of_ids, NUMBER_OF_RANDOM_ADS)


def create_ads(list_of_ad_ids):
    for id in list_of_ad_ids:
        fetch_ad_and_store(id)


if __name__ == '__main__':
    log.info("Start")
    list_of_ids = get_ad_ids()
    log.info(f" {len(list_of_ids)} ads are currently published")
    ids_to_add_as_mock_ads = random_ad_ids(list_of_ids)
    if EXTRA_AD_IDS:
        ids_to_add_as_mock_ads.extend(EXTRA_AD_IDS)
    de_duplicated_ids = set(ids_to_add_as_mock_ads)
    log.info(f"Creating {len(de_duplicated_ids)} ads")
    create_ads(de_duplicated_ids)
    log.info("Finished")
