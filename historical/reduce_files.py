import os
import glob
import json
import random

percent = 1


def write_json_file(data, year):
    filename = f"{year}_short.json"
    print(f"Writing {filename}")
    with open(filename, 'w') as f:
        json.dump(data, f, ensure_ascii=False)


def select_random_ads(all_ads, percent):
    percentage = percent / 100
    desired_number = int(len(all_ads) * percentage) + 1
    return random.sample(all_ads, desired_number)


def start():
    total_ads = 0
    json_files = glob.glob("*.json")
    for f in json_files:
        year = os.path.splitext(f)[0]
        print(f"Loading {f}")
        with open(f, 'r') as file:
            list_of_ads_from_file = json.load(file)

        random_ads = select_random_ads(list_of_ads_from_file, percent)
        print(f"{len(random_ads)} random ads from {year}")
        write_json_file(random_ads, year)
        total_ads += len(random_ads)

    print('Total random ads: ', total_ads)


if __name__ == '__main__':

    start()
