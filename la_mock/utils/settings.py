import os
from distutils.util import strtobool

LA_BOOTSTRAP_FEED_URL = os.getenv('LA_BOOTSTRAP_FEED_URL', ' ')
LA_DETAILS_URL = os.getenv('LA_DETAILS_URL', '')

# use ads that matches jobsearch api tests if true, otherwise ad files in "compare_ads"
TEST_ADS = strtobool(os.getenv("TEST_ADS", "true"))
