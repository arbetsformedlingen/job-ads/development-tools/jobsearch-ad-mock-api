import os
import json
from multiprocessing import Pool
import glob
import pathlib
import logging

import requests

import settings

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

SAVE_PATH = pathlib.Path.cwd() / "downloaded_ads"

os.makedirs(SAVE_PATH, exist_ok=True)

"""
This program does the following:
- looks in SAVE_PATH to see if any ad files are already downloaded
- gets id:s for currently published ads from Ledigt Arbete
- start download in parallel of ads that are not already downloaded

Errors are ignored. If you run it again, it will not download already downloaded ads.

Downloaded ads needs to be anonymized.

"""


def get_ids_from_saved_files() -> list:
    """
    If there is a problem when downloading from Ledigt Arbete, the already downloaded ads will not be downloaded again
    """
    log.info("getting ids from already downloaded files")
    ids = []
    files = glob.glob("async_ads\\*.json")
    for f in files:
        tmp_name = f.split("\\")[1].split(".")[0]
        ids.append(tmp_name)
    log.info(f"Already saved: {len(ids)} ads")
    return ids


def get_ad_ids() -> list:
    log.info("get ad ids for published ads")
    response = requests.get(settings.LA_BOOTSTRAP_FEED_URL, timeout=3)
    response.raise_for_status()
    list_of_current_ads = json.loads(response.content.decode('utf8'))['idLista']
    list_of_ids = []
    log.info(f"got {len(list_of_current_ads)} ids")
    for ad in list_of_current_ads:
        if not ad['avpublicerad']:
            list_of_ids.append(str(ad['annonsId']))
    log.info(f"{len(list_of_ids)} ids are published")
    return list_of_ids


def ids_to_get(ids_from_la: list, ids_from_files: list) -> list:
    ids_to_process = set(ids_from_la) - set(ids_from_files)
    log.info(f"Ids from LA: {len(ids_from_la)}, already saved: {len(ids_from_files)}. To do: {len(ids_to_process)}")
    return list(ids_to_process)


def save_to_file(ad: dict) -> None:
    filename = SAVE_PATH / f"{ad['id']}.json"
    with open(filename, 'w') as f:
        json.dump(ad, f)
    log.info(f"Saved {filename}")


def get_ad(ad_id: str) -> None:
    """
    Downloads ad from LA details endpoint, errors are logged but ignored
    """
    ad_url = settings.LA_DETAILS_URL + ad_id
    try:
        response = requests.get(ad_url, timeout=2)
        response.raise_for_status()
        ad = json.loads(response.content.decode('utf8'))
        save_ad(ad)
    except Exception as e:
        log.error(f"{ad_id}: {e}")


def save_ad(ad: dict) -> None:
    try:
        filename = SAVE_PATH / f"{ad['annonsId']}.json"
        with open(filename, 'w') as f:
            json.dump(ad, f)
        log.debug(f"Saved {filename}")
    except Exception as e:
        log.error(f"{ad['annonsId']}. Error: {e}")


def id_finder() -> list:
    list_of_ids_from_la = get_ad_ids()
    list_of_ids_from_file = get_ids_from_saved_files()
    ids_to_process = ids_to_get(list_of_ids_from_la, list_of_ids_from_file)
    return ids_to_process


def working_pool() -> None:
    log.info("start downloading all ads")
    ids_to_process = id_finder()
    p = Pool(4)
    p.map(get_ad, ids_to_process)
    p.close()
    log.info("Finished")


if __name__ == '__main__':
    working_pool()
