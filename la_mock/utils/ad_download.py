# -*- coding: utf-8 -*-
from la_mock.utils.anonymize.anonymize import mask_sensitive_info
from la_mock.utils import settings

import requests
import argparse
import json
import time
import logging

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

parser = argparse.ArgumentParser()
parser.add_argument('id', help="The id of the ad you want to fetch")


def fetch_ad_and_store(id):
    ad_url = settings.LA_DETAILS_URL + str(id)
    log.info(f'fetching ad {ad_url}')
    retries = 5
    for i in range(1, retries):
        try:
            response = requests.get(ad_url, timeout=1)
            response.raise_for_status()
            ad = json.loads(response.content.decode('utf8'))
        except:
            log.warning(f"retry {i} for {ad_url}")
            if i == retries:
                log.error(f"ignoring ad {ad_url}")
                return
            time.sleep(1)
        else:
            # OK response
            break


    #cleaned_ad = _anonymize_LA_ad(ad)
    _save_ad(ad)
    return


def _anonymize_LA_ad(ad):
    log.debug(f"Cleaning ad {ad['annonsId']}")
    ad['annonstext'] = mask_sensitive_info(ad['annonstext'])
    ad['annonstextFormaterad'] = mask_sensitive_info(ad['annonstextFormaterad'])
    ad['informationAnsokningssatt'] = mask_sensitive_info(ad['informationAnsokningssatt']) if ad.get(
        'informationAnsokningssatt') else None
    ad['telefonnummer'] = mask_sensitive_info(ad['telefonnummer']) if ad.get('telefonnummer') else None
    ad['ansokningssattEpost'] = mask_sensitive_info(ad['ansokningssattEpost']) if ad.get(
        'ansokningssattEpost') else None
    ad['kontaktpersoner'] = [_fake_contact_person()]
    return ad


def _fake_contact_person(fack=False):
    return {"fornamn": "Testy",
            "efternamn": "Testsson",
            "befattning": None,
            "telefonnummer": "+01011122233",
            "epost": "test@jobtechdev.se",
            "fackligRepresentant": fack,
            "beskrivning": "blabla"}


def _save_ad(ad):
    file_name = settings.AD_PATH / f"{ad['annonsId']}.json"
    with open(file_name, 'w') as f:
        json.dump(ad, f)
    log.info(f"Saved ad {ad['annonsId']} to {settings.AD_PATH}")

def main():
    args = parser.parse_args()
    fetch_ad_and_store(args.id)


if __name__ == "__main__":
    main()
