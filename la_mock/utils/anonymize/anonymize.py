# -*- coding: utf-8 -*-
import re
import collections
import os


def _build_dictionary_lastname():
    # build Trie Dictionary for last name
    dictionary_lastname = collections.defaultdict()
    with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'last_name.txt'), 'r', encoding='utf-8') as file:
        for line in file.readlines():
            dic = dictionary_lastname
            for w in line.rstrip("\n"):
                dic = dic.setdefault(w, {})
            dic.setdefault('_end')
    return dictionary_lastname


def _build_dictionary_firstname():
    # build Trie Dictionary for first name
    dictionary_firstname = collections.defaultdict()
    with open(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'first_name.txt'), 'r', encoding='utf-8') as file:
        for line in file.readlines():
            dic = dictionary_firstname
            for w in line.rstrip("\n"):
                dic = dic.setdefault(w, {})
            dic.setdefault('_end')
    return dictionary_firstname


def _check_name(word: str, dictionary):
    # Check if it is first name in first name dictionary, or if it is last name in last name dictionary.
    # If it is return true if it is not return False
    name = dictionary
    for letter in word:
        if letter not in name:
            return False
        name = name[letter]
    if "_end" in name:
        return True
    return False


def _find_name(ad_text: str):
    # Find full name and mask name
    ads = re.split(r'(\W)', ad_text)
    length = len(ads)
    dictionary_firstname = _build_dictionary_firstname()
    dictionary_lastname = _build_dictionary_lastname()
    i = 0
    while i < length:
        if _check_name(ads[i], dictionary_firstname):
            next = i + 2
            if next > length:
                ads[i] = 'FIRSTNAME'
            elif _check_name(ads[next], dictionary_lastname):
                ads[i] = 'FIRSTNAME'
                ads[next] = 'LASTNAME'
        i += 1
    return ''.join(ads)


def mask_sensitive_info(ad) -> str:
    email_regex = r'[\w\.-]+@([\w-]+\.)+[\w-]{2,4}'
    personal_no_regex = '[19|20]{0,2}[0-9][0-9](0[1-9]|1[0-2])(0[1-9]|1[1-9]|2[1-9]|3[0|1])[-|(\\s)]{0,1}[T|\\d]\\d{3}'
    telephone_regex = r'[(]{0,1}[00|\+]{0,2}[0-9]{1,4}[)]{0,1}\s*[0]{0,1}[1-9]\s*[-]{0,1}\s*[0-9]\s*[-]{0,1}\s*[0-9]\s*[-]{0,1}\s*[0-9]\s*[0-9]\s*[0-9]\s*[0-9]\s*[0-9]{0,1}\s*[0-9]{0,1}\s*'
    result = re.sub(email_regex, ' EMAIL ', ad)
    result = re.sub(personal_no_regex, ' PERSONALNO ', result)
    result = re.sub(telephone_regex, ' TELEPHONENO ', result)
    result = _find_name(result)
    return result
