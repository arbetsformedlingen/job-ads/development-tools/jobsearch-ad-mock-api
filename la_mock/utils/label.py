# -*- coding: utf-8 -*-
import json
import os
import random
from typing import List
import click


LABEL_FIELD = "nyckelord"


def get_ad_filenames(directory):
    return [
        entry.path
        for entry in os.scandir(directory)
        if entry.is_file() and entry.name.endswith(".json")
    ]


def add_label_to_file(filename, label):
    with open(filename, "r") as f:
        data = json.load(f)

    if LABEL_FIELD in data and isinstance(data[LABEL_FIELD], List):
        data[LABEL_FIELD].append(label)
    else:
        data[LABEL_FIELD] = [label]

    with open(filename, "w") as f:
        f.write(json.dumps(data))


def set_label_field_to_value(filename, value):
    with open(filename, "r") as f:
        data = json.load(f)

    data[LABEL_FIELD] = value

    with open(filename, "w") as f:
        f.write(json.dumps(data))


def clear_labels_from_file(filename):
    set_label_field_to_value(filename, [])


def erase_label_field_from_file(filename):
    with open(filename, "r") as f:
        data = json.load(f)

    del data[LABEL_FIELD]

    with open(filename, "w") as f:
        f.write(json.dumps(data))


def read_labels_from_file(filename):
    with open(filename, "r") as f:
        data = json.load(f)

    rv = data.get(LABEL_FIELD, [])

    if isinstance(rv, List):
        return rv

    return [str(rv)]


@click.group()
def label():
    """Handle the label field in the JSON files that LA mock presents as ads."""


@label.command()
@click.argument("directory", type=click.Path(exists=True, dir_okay=True))
def randomize(directory):
    """Apply a set of changes randomly to the label field for all .json files in directory."""

    click.echo("Listing files...")
    filenames = get_ad_filenames(directory)
    file_count = len(filenames)
    click.echo("Found {} files.".format(file_count))

    click.echo("Clearing labels...")
    for filename in filenames:
        clear_labels_from_file(filename)

    click.echo("Adding labels NYSTARTSJOBB to 5% of files...")
    for filename in random.sample(filenames, int(file_count * 0.05)):
        add_label_to_file(filename, "NYSTARTSJOBB")

    click.echo("Adding label REKRYTERINGSUTBILDNING to 5% of files...")
    for filename in random.sample(filenames, int(file_count * 0.05)):
        add_label_to_file(filename, "REKRYTERINGSUTBILDNING")

    click.echo("Adding label DUMMY_LABEL to 1% of files...")
    for filename in random.sample(filenames, int(file_count * 0.01)):
        add_label_to_file(filename, "DUMMY_LABEL")

    click.echo("Setting label field to None for 1% of files...")
    for filename in random.sample(filenames, int(file_count * 0.01)):
        set_label_field_to_value(filename, None)

    click.echo("Erasing label field from 1% of files...")
    for filename in random.sample(filenames, int(file_count * 0.01)):
        erase_label_field_from_file(filename)

    click.echo("\nDone! 🎉")


@label.command()
@click.option("-p", "--probability", default=0.05, help="Probability to add label.")
@click.argument("directory", type=click.Path(exists=True, dir_okay=True))
@click.argument("label", type=click.STRING)
def add(directory, label, probability):
    """Add a label to a random sample of .json files in directory."""

    click.echo("Listing files...")
    filenames = get_ad_filenames(directory)
    file_count = len(filenames)
    click.echo(f"{file_count} files found.")

    click.echo("Labeling files...")
    sample_size = int(file_count * probability)
    for filename in random.sample(filenames, sample_size):
        click.echo(f"Adding {label} to {filename}")
        add_label_to_file(filename, label)

    click.echo("\nDone! 🎉")


@label.command()
@click.argument("directory", type=click.Path(exists=True, dir_okay=True))
def clear(directory):
    """Set the label field to an empty list in all .json files in directory."""

    click.echo("Listing files...")
    filenames = get_ad_filenames(directory)
    file_count = len(filenames)
    click.echo(f"{file_count} files found.")

    click.echo("Removing labels from files...")
    for filename in filenames:
        click.echo(f"Removing label from {filename}")
        clear_labels_from_file(filename)

    click.echo("\nDone! 🎉")


@label.command()
@click.argument("directory", type=click.Path(exists=True, dir_okay=True))
def erase(directory):
    """Remove the label field from all .json files in directory."""

    click.echo("Listing files...")
    filenames = get_ad_filenames(directory)
    file_count = len(filenames)
    click.echo(f"{file_count} files found.")

    click.echo("Removing labels from files...")
    for filename in filenames:
        click.echo(f"Removing label from {filename}")
        clear_labels_from_file(filename)

    click.echo("\nDone! 🎉")


@label.command()
@click.argument("directory", type=click.Path(exists=True, dir_okay=True))
def histogram(directory):
    """Print a histogram of labels in all .json files in directory."""

    click.echo("Listing files...")
    filenames = get_ad_filenames(directory)
    file_count = len(filenames)
    click.echo(f"{file_count} files found.")

    click.echo("Counting label values...\n")
    label_count = {}
    for filename in filenames:
        labels_from_file = read_labels_from_file(filename)

        labels_as_string = "+".join(sorted(labels_from_file)) or "empty"
        label_count[labels_as_string] = label_count.get(labels_as_string, 0) + 1

    for key, value in sorted(
        label_count.items(), key=lambda item: item[1], reverse=True
    ):
        click.echo(f"{value:>6}: {key.replace('+', ' & ')}")

    click.echo("\nDone! 🎉")


@label.command()
@click.argument("directory", type=click.Path(exists=True, dir_okay=True))
@click.argument("labels", type=click.STRING, nargs=-1)
def list(directory, labels):
    """Print names of files that contain all of the provided labels."""

    click.echo("Listing files...")
    filenames = get_ad_filenames(directory)
    file_count = len(filenames)
    click.echo(f"{file_count} files found.")

    click.echo(f"Listing all files containing labels {' and '.join(labels)}")
    for filename in filenames:
        labels_from_file = read_labels_from_file(filename)

        if all(label in labels_from_file for label in labels):
            click.echo(filename)

    click.echo("\nDone! 🎉")


if __name__ == "__main__":
    label()
