import os
import glob
import json
import datetime
import logging
from la_mock.utils import settings

log = logging.getLogger(__name__)
logging.basicConfig(level=logging.INFO)

ads_dir = "test_ads" if settings.TEST_ADS else "compare_ads"
ad_file_path = os.path.join(os.path.abspath(os.path.dirname(__file__)) + f'/resources/{ads_dir}')
log.info(f"Test mode: {settings.TEST_ADS}, using ads from {ad_file_path}")


def read_ad_file(file) -> dict:
    with open(file, encoding="utf8") as ad:
        return json.load(ad)


def fetch_ad_files():
    log.info(f"Reading ad files")
    ads = {}
    for file in glob.glob(os.path.join(ad_file_path, '*.json')):
        annons = read_ad_file(file)
        # save memory by only including basic ad data
        annonsId = annons['annonsId']
        new_ad = {
            'annonsId': annonsId,
            'sistaPubliceringsdatum': annons['sistaPubliceringsdatum'],
            'avpublicerad': annons['avpublicerad'],
            'uppdateradTid': annons['uppdateradTid'],
            'publiceringsdatum': annons['publiceringsdatum']
        }
        ads[annonsId] = new_ad
    log.info(f"Loaded {len(ads)} ads")
    return ads


def _set_depublishing_date(ad):
    # Sets the date to unpublish an ad to one year in the future from current date
    ad['sistaPubliceringsdatum'] = datetime.datetime.strftime(datetime.datetime.now() + datetime.timedelta(days=365),
                                                              '%Y-%m-%d %H:%M:%S')
    ad['avpublicerad'] = False
    ad.pop('avpubliceringsdatum', None)
    return ad


def fetch_changed_ad_ids(ads):
    log.info("fetch_changed_ad_ids")
    ads_values = ads.values()
    changed_ads = {"idLista": [{'annonsId': ad['annonsId'], 'uppdateradTid': ad['uppdateradTid'],
                                'avpublicerad': True} for ad in ads_values]}
    return changed_ads


def fetch_ad_ids(ads):
    log.info("fetch_ad_ids")
    ad_ids = {"idLista": [{'annonsId': ad['annonsId'], 'uppdateradTid': ad['uppdateradTid'],
                           'avpublicerad': False} for ad in list(ads.values())]}
    return ad_ids


def read_ad_from_file(ad_id: int) -> dict:
    # Used by /annonser/<annonsId> endpoint
    file = ad_file_path + f"/{ad_id}.json"
    ad = read_ad_file(file)
    ad = _set_depublishing_date(ad)
    return ad
