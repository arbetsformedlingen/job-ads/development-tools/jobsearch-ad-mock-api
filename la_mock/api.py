from flask import Flask, jsonify, abort

from . import queries

# Read all the ads from files
ads = queries.fetch_ad_files()

app = Flask(__name__)


@app.route('/sokningar/publiceradeannonser')
def fetch_ads():
    # Mocks the LA endpoint that returns a list of ads that are published now
    la_formatted_adlist = queries.fetch_ad_ids(ads)
    return jsonify(la_formatted_adlist)


@app.route('/annonser/<annonsId>')
def search_ad(annonsId):
    # Mocks the LA endpoint that returns details about an ad
    la_formatted_ad = queries.read_ad_from_file(annonsId)
    return jsonify(la_formatted_ad) if la_formatted_ad else abort(404)


@app.route('/sokningar/andradeannonser/<timestamp>')
def updated_ads(timestamp):
    # Mocks the LA endpoint that returns a list of ads that has changed status
    # (i.e. been deleted) from the timestamp until now as interval
    updated_adlist = queries.fetch_changed_ad_ids(ads)
    return jsonify(updated_adlist)
