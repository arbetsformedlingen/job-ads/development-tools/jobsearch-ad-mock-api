FROM  docker.io/library/python:3.12.7-slim-bookworm

ARG POETRY_VERSION=1.7.1

# uwsgi won't run without libexpat.so.1
RUN apt-get -y update && \
    apt-get install --no-install-recommends -y libexpat1 && \
    rm -rf /var/lib/apt/lists/*

RUN pip install poetry==${POETRY_VERSION}

WORKDIR /app

COPY poetry.lock pyproject.toml /app/

RUN poetry config virtualenvs.create false && \
    poetry install --only main --no-root --no-interaction --no-ansi

COPY ./la_mock /app/la_mock/

USER 10000

EXPOSE 8081

CMD  ["uwsgi", "--http", ":8081", "--manage-script-name", "-w", "la_mock.api:app"]
